package org.sakaiproject.tool.assessment.services;

public class SaLengthException extends RuntimeException {
	/**
	 * Creates a new SaLengthException object.
	 *
	 * @param message DOCUMENTATION PENDING
	 */
	public SaLengthException(String message)
	{
		super(message);
	}
}
